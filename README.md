# Aula5

**Fundamentos do teste de software(Back-End)**

Com o ciclo de desenvolvimento, você ja começa a pensar que tipo de metodologia ou técnica vamos aplicar para melhor qualidade de projeto, com isso conseguimos antecipar os testes para o quanto antes pegar os bugs pegar os erros, para que não chegue ao cliente com erros.

**Pirâmede de Testes:**

**Teste Unitário -**
Ele fica na base da Pirâmede, ele é visto como um método publico em uma classe, quando falamos em orientação a obejetos, mas pode ser vista também como conjuntos de classe, métodos e objetos que interagem entre si.

**Os testes:** de unidade verificam o funcionamento da menor unidade de código, testado da nossa aplicação independente da interação dela com as outras partes do nosso código, ou seja não precisamos ter um código completo.

------------

**Teste de Integração** - Ele avalia a funcionalidade de vários módulos quando integrados para formar uma unica unidade, este teste valida a transição suave entre vários componentes integrados no software.

**O objetivo:** do teste de integração é encontrar defeitos e falhas entre varias interfaces do software.

--------------

**Teste E2E** - São os testes de ponta a ponta que simulam um ambiente real, isto é sobem aplicação ou você abre o navegador preenche formulários, clica em botões verifica se aconteceu o que era esperado, ou seja você vai do início até o final testanto sua aplicação.

**EX:** Site Riachuelo
* Cadastrar
* Escolher item 
* Calcular frete 
* Sacola 
* Pagamento
* Finalizar pedido
* Informações do pedido
* Boleto
* Meus pedidos  

